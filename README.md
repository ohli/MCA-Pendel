# Projektarbeit LED-Doppelpendel

Modul Microcontroller-Anwendungen bei Prof. Jens Wagner, Sommersemester 2018, HTWK Leipzig

## Software
* Platinenlayout: [Fritzing](http://fritzing.org/)
* Programmierung: [AVRStudio](http://www.microchip.com/avr-support/atmel-studio-7)

## Links
* [AVR-Timer](https://www.mikrocontroller.net/articles/AVR-Tutorial:_Timer)
* [AVR-Interrupts](https://www.mikrocontroller.net/articles/AVR-GCC-Tutorial#Programmieren_mit_Interrupts)
