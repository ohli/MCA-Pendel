# Projekt Pendel-Controller WS 2017/18
## Modul Mikrocontrolleranwendungen, Prof. J. Wagner


|Problem|Methode|Lösung|
|-------|-------|------|
|Timer nicht synchron | Auslesen der Timerzustände über Oszi | Timer 0 und 2 über globale Zählvariablen synchronisieren |
|Debugger nicht funktionell |JTAG-ICE & µC vor Debugging resetten | siehe nächste Zeile |
|Keine serielle Ausgabe | UART-Interface implementieren, Aufwand vs. Nutzen? | Debugging über Port schalten und Oszilloskop |
|Spannungsabfall bei weißen LEDs | GRB-Leds auf Max. (255,255,255) setzen, U_Led messen | LEDs absichern, Summe der Farbcodes beachten |
|initialer Zustand über Spule nicht stabil | Oszi an Spule| Zeitraum zum "Einschwingen" des Pendels |
|Massenschleifen| Spannung an +5V und +12V im Ruhezustand messen, prüfe auf Störungen | Trennung von GND und AGND, sternförmige Massepunkte |
|µC absichern| Verpolung/Überspannung nicht ausgeschlossen | Z-Diode und C an V++ und GND des µC |
|Pendelschwingung über Spule messen| Vergleich der Flanken am Oszi | herausfiltern negativer Flanken, Schwellwert |
|Werkskalibrierung des µC nicht vorhanden| Flash auslesen | Frequenz softwareseitig vorgeben |
|Masseschwerpunkt des Pendels zu klein| Pendel schwingt nicht richtig | Schraube an Magneten des Pendels anbringen |
|keine Versionsverwaltung auf Laborrechner| evtl. Upload per Webinterface | USB-Stick mit neuen Namen für "gebrachte" Versionen (z.B. led-timer)  |
|Rauschen am OPV| Signal des OPV im Ruhezustand auslesen | Glättungskondensatoren, Puffer-OPV |
|Schaltung zu groß| Verteilung auf zwei Steckplatinen unnötig |Transistoren durch MOSFETs ersetzen da spannungsgesteuert|
|Timing der LEDs suboptimal| Berechnung der Umschaltzeit der LEDs | 125µs * Anz. LEDs, Interrupts in LED-Funktion deaktivieren |
|Variablen verschwinden bei Kompilierung| Variablenzugriff im zeitl. Kontext betrachten | Variablen als volatile deklarieren, `CFLAGS = -Os` |
|Spannungsspitzen durch Spule| Magnetfeld in Spule wird zurückinduziert, da Magnetfeld auf- und abgebaut werden muss | Freilaufdiode |
|Anregung der Spule funktioniert nicht| Magnetfeld über Spule messen | Spule versetzt zu Magneten positionieren, initiale Anregung Spule dauert länger |
|Spannung durch Magnetfeld über Spule zu gering|  induzierte Spannung hinter Spule bei Erregung messen | Induktionsspannung mittels OPV verstärken|
|Pendelrichtung unvorhersehbar| laut Aufgabenstellung |Pendel breiter aufhängen|
|Periodendauer des Pendels ungewiss| Messen des Nulldurchgangs des Pendels |adaptive Periodendauermessung, Annäherung der Periodendauer |
|Störeinflüsse auf Schaltung | z.B. JTAG-Interface bringt Störung durch PC ein | Abstand halten, bei Nichtgebrauch abklemmen |
|Schaltung unübersichtlich | Schaltplan erstellen | rot für VCC, schwarz/blau für Masse | 


