#include <avr/io.h>
#include <avr/interrupt.h>

/*Compiler-Options:
 avr-gcc  -mmcu=atmega16 -Wall -gdwarf-2 -std=gnu99 
-DF_CPU=8000000UL -O3 -funsigned-char -funsigned-bitfields 
-fpack-struct -fshort-enums -MD -MP -MT einfbsp.o -MF 
dep/einfbsp.o.d  -c  ../einfbsp.c
*/

#ifndef F_CPU
/* Definiere F_CPU, wenn F_CPU nicht bereits vorher definiert 
   (z.B. durch �bergabe als Parameter zum Compiler innerhalb 
   des Makefiles). Zus�tzlich Ausgabe einer Warnung, die auf die
   "nachtr�gliche" Definition hinweist */
#define F_CPU 8000000UL     // 8 Mhz
#endif

#include <util/delay.h>


#define BAUD 9600
#include <util/setbaud.h>

void uart_init(void)   
{
   UBRRH = UBRRH_VALUE;
   UBRRL = UBRRL_VALUE;
   /* evtl. verkuerzt falls Register aufeinanderfolgen (vgl. Datenblatt)
      UBRR = UBRR_VALUE;
   */
#if USE_2X
   /* U2X-Modus erforderlich */
   UCSRA |= (1 << U2X);
#else
   /* U2X-Modus nicht erforderlich */
   UCSRA &= ~(1 << U2X);
#endif

   // hier weitere Initialisierungen (TX und/oder RX aktivieren, Modus setzen 
}




/*
 lange, variable Verz�gerungszeit, Einheit in Millisekunden

Die maximale Zeit pro Funktionsaufruf ist begrenzt auf 
262.14 ms / F_CPU in MHz (im Beispiel: 
262.1 / 3.6864 = max. 71 ms) 
*/

#define nop() asm volatile("nop")


//Taktbereinigte ZERO und ONE
#define ZEROl { PORTB=0xFF;\
				nop();nop();\
				PORTB=0x0;\
				nop();nop();nop();nop();nop();nop();}


#define ONEl { 	PORTB=0xFF;\
				nop();nop();nop();nop();nop();\
				PORTB=0x0;\
				nop();nop();nop();}

// Anzahl LEDS auf Streifen
#define NUMLED 15

// globale Variablen
volatile int timer0count=0, set=0;
volatile int timer1count=0;
volatile int timer2count=0;
volatile int timerflag=0;
volatile int ausschlag=0;


// Funktionen
void leuchte(int g, int r, int b) 
{
	if (g&1) ONEl else ZEROl
	if (g&2) ONEl else ZEROl
	if (g&4) ONEl else ZEROl
	if (g&8) ONEl else ZEROl
	if (g&16) ONEl else ZEROl
	if (g&32) ONEl else ZEROl
	if (g&64) ONEl else ZEROl
	if (g&128) ONEl else ZEROl
	
	if (r&1) ONEl else ZEROl
	if (r&2) ONEl else ZEROl
	if (r&4) ONEl else ZEROl
	if (r&8) ONEl else ZEROl
	if (r&16) ONEl else ZEROl
	if (r&32) ONEl else ZEROl
	if (r&64) ONEl else ZEROl
	if (r&128) ONEl else ZEROl

	if (b&1) ONEl else ZEROl
	if (b&2) ONEl else ZEROl
	if (b&4) ONEl else ZEROl
	if (b&8) ONEl else ZEROl
	if (b&16) ONEl else ZEROl
	if (b&32) ONEl else ZEROl
	if (b&64) ONEl else ZEROl
	if (b&128) ONEl else ZEROl
}

void ledaus (void) 
{
	int i;
	for(i=0; i<NUMLED; i++) {
		leuchte(0,0,0);
	}
	_delay_us(50);
}

void farbwechsel (void)
{
	// rot
	leuchte(0,255,0);
	// orange
	leuchte(127,255,0);
	// gelb
	leuchte(255,255,0);
	// gr�ngelb
	leuchte(255,127,0);
	// gr�n
	leuchte(255,0,0);
	// blaugr�n
	leuchte(255,0,127);
	// cyan
	leuchte(255,0,255);
	// blau
	leuchte(0,0,255);
}

void allesrot (void)
{
	int i;
	for(i=0; i<NUMLED; i++) {
		leuchte(0,255,0);
	}
}

void allesgelb (void)
{
	int i;
	for(i=0; i<NUMLED; i++) {
		leuchte(111,111,0);
	}
}

void allesblau (void)
{
	int i;
	for(i=0; i<NUMLED; i++) {
		leuchte(0,0,255);
	}
}

void allesgruen (void)
{
	int i;
	for(i=0; i<NUMLED; i++) {
		leuchte(255,0,0);
	}
}

void dreifarbe (void)
{
	int i;
	for (i=0; i<NUMLED/3; i++) {
		leuchte(255,0,0);
		leuchte(0,255,0);
		leuchte(0,0,255);
	}
}


// int time : Zeit in ms
void blinke (int time)
{
		allesrot();
		_delay_ms(time);
		allesgruen();
		_delay_ms(time);
		allesblau();
		_delay_ms(time);
}


void setupTimer (void)
{
	// Timer0 mit 8 Bit f�r LED-Trigger
	// Prescaler 1024, also 7,8 kHz
	// OVF nach 0,033s
	TCCR0 |= 0x03;  // 1024 //setze CS00
	
	// Timer1 mit 16 Bit f�r Pendellaenge
	// Prescaler 1024, also 7,8 kHz
	// OVF nach 8,38s
	//TCCR1B |= 0x05;  //setze CS10 und CS12

	// Timer2 mit 8 Bit f�r LED-Trigger
	// Prescaler 1024, also 7,8 kHz
	// OVF nach 0,033s
	TCCR2 |= 0x03;  //1024			//setze CS00

	// Overflow-Interrupts aktivieren
	TIMSK |= (1<<TOIE0);	//aktiviere timer0 Overflow Interrupt
	//TIMSK |= (1<<TOIE1);
	TIMSK |= (1<<TOIE2);	//aktiviere timer2 Overflow Interrupt
}

void init (void)
{
	// Geschwindigkeit kalibrieren, da im Flash gel�scht!
    OSCCAL = 0xA7;
	_delay_ms(20);

	// Fuer LED
	// Alle Pins auf 1, also Ausgang
	DDRB  = 0xFF;
	PORTB = 0xFF;
	
	// Debug-Port
	DDRC = 0xFF;
	PORTC = 0x0;

	// Fuer Spule aus
	DDRD  = 1;
	PORTD = 0;

	// Interrupts an INT0 aktivieren
	//GICR   |= (1<<INT0);
	// Interrupt bei steigender Flanke
	//MCUCSR |= (1<<ISC00);
	//MCUCSR |= (1<<ISC01);

	
	//SFIOR &= 0b00011111; // freerun	als trigger (ADTS0-2)
	// ADMUX &= 0b00111111; // AREF enable
	//ADMUX |= 0b00000001; ADMUX &= 0b11111001; // ADC1 ausw�hlen 
	//ADMUX |= 0b00001000; ADMUX &= 0b11101111; // 10x gain
	ADMUX |= (1<<REFS0); //aref avcc

	//ADCSRA |= (1<<ADATE); // autotrigger enable
	//ADCSRA &= 0b11111110; ADCSRA |= 6; // Prescaler auf 64 setzen 8MHz/64=125kHz ADC
	//ADCSRA |= (1<<ADEN); // ADC enable
	ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0); // XTAL/128 f�r ADC clock
	// -> 8MHz/128 = 62,5kHz = fa; 50kHz < fa < 200kHz -> optimal f�r maximale Aufl�sung der skuzessive Approximation

	// Ruhezustand init
	ADMUX  |= 0b00000001; ADMUX  &= 0b11111001; // ADC1 ausw�hlen 
	//ADCSRA |= (1<<ADSC);

}

#define ANZSCHWANKUNGEN 33 // 100ms messen

int main (void)
{
	init();
	
	dreifarbe();
	_delay_ms(5000);
	
	// init schwankung
	int i, schwankung[ANZSCHWANKUNGEN], schwelle=0;
	
	for (i=0; i<ANZSCHWANKUNGEN; i++) {
		ADCSRA |= (1<<ADSC);
		while (ADCSRA & (1<<ADSC));
		schwankung[i]=ADC;
		_delay_ms(3);
	}
	for (i=0;i<ANZSCHWANKUNGEN; i++) {
		schwelle=schwelle+schwankung[i];
	}
	schwelle=schwelle/ANZSCHWANKUNGEN;

	
	// max Abstand von Mittelwert schwelle
	int delta=0;
	for (i=0; i<ANZSCHWANKUNGEN; i++) {
		if (schwelle-schwankung[i]<0) {
			if (schwankung[i]-schwelle > delta) {
				delta=schwankung[i]-schwelle;
			}
		} else {
			if (schwelle-schwankung[i] > delta) {
				delta=schwelle-schwankung[i];
			}
		}
	}
	delta+=5;
	

	
	// zeige Startzustand an
	allesrot();

	// Werte f�r Kurvenerkennung initialisieren
	int aktuell=0, alt[3];

	ADCSRA |= (1<<ADSC);
	while (ADCSRA & (1<<ADSC));
	alt[2]= ADC;

	ADCSRA |= (1<<ADSC);
	while (ADCSRA & (1<<ADSC));
	alt[1]= ADC;

	ADCSRA |= (1<<ADSC);
	while (ADCSRA & (1<<ADSC));
	alt[0]= ADC;

	// Spule von Nullzustand anregen
	PORTD=1;
	_delay_ms(300);
	PORTD=0;
	_delay_ms(600);

	// init Timer
	setupTimer();
	//Interrupts aktivieren
	sei();
	int startverzoegerung=4;

	// Spule messen und weiter anregen
	while(1) {
		// ADC lesen
		ADCSRA |= (1<<ADSC);
		while (ADCSRA & (1<<ADSC));
		aktuell = ADC;
		

		cli();
		// fallende Kurve erkennen
		if (aktuell < alt[0] && aktuell < alt[1] && aktuell < alt[2] && aktuell < schwelle-delta) {
			if (startverzoegerung) {
				startverzoegerung--;
				TCNT2=0;
				TCNT0=0;
				timer0count=0;
				timer2count=0;
			}

			//LED-Zeitpunkt berechnen
			if (!startverzoegerung) {
				TCNT2=0;
				TCNT0=0;
				timer2count = timer0count/2;
				timer0count=0;
				set=0;
				if (ausschlag) 	allesgelb(); // links
				else			allesgruen();// rechts
			}
			sei();
			// Spule feuern
			PORTD=1;
			_delay_ms(40);
			PORTD=0;
			//_delay_ms(430);
			_delay_ms(100);

		} else {
			// schreibe neue Historie
			alt[2]=alt[1];
			alt[1]=alt[0];
			alt[0]=aktuell;
		}
		sei();
		
		// schlie�e hochfrequente Schwankungen im Eingangssignal durch Verz�gerung aus
		// -> fallende Kurven mit hohen Frequenzen werden nicht erkannt
		//dreifarbe();
		_delay_ms(5);
	}
}

ISR( TIMER0_OVF_vect ) {
	timer0count++;
	/*if (timer0count>1000) {
			dreifarbe();
			while(1);
		
	}*/
}

ISR( TIMER2_OVF_vect ) {
	if (timer2count==0 && set==0) {
		if (ausschlag) 	allesrot(); // links
		else			allesblau();// rechts
		ausschlag^=1;
		set=1;
	} else {
		timer2count--;
	}
}
